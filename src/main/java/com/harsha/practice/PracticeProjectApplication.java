package com.harsha.practice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.harsha.practice.repositories.MovieMySQLwithJPARepository;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = MovieMySQLwithJPARepository.class)
public class PracticeProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticeProjectApplication.class, args);
	}

}
