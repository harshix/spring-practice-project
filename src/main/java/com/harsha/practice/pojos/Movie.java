package com.harsha.practice.pojos;

import java.util.HashMap;

public class Movie {

	private HashMap<String, String> cast;
	private Credits credits;

	public HashMap<String, String> getCast() {
		return cast;
	}

	public void setCast(HashMap<String, String> cast) {
		this.cast = cast;
	}

	public Credits getCredits() {
		return credits;
	}

	public void setCredits(Credits credits) {
		this.credits = credits;
	}
	
	
	
	
}
