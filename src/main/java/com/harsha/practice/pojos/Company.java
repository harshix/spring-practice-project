package com.harsha.practice.pojos;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Company {
	
	@Id
	private String name;
	private String industry;
	
}
