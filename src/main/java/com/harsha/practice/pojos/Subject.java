package com.harsha.practice.pojos;

public class Subject {

	private String name;
	private String teacher;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTeacher() {
		return teacher;
	}
	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}
	
	public Subject(String name, String teacher) {
		super();
		this.name = name;
		this.teacher = teacher;
	}
	
	
}
