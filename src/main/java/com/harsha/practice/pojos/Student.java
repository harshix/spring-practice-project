package com.harsha.practice.pojos;

public class Student {

	private int id;
	private String name;
	private int rank;
	private Course course;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public Student(int id, String name, Course course, int rank) {
		super();
		this.id = id;
		this.name = name;
		this.course = course;
		this.rank = rank;
	}
	
	
}
