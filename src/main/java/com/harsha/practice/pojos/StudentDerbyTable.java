package com.harsha.practice.pojos;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class StudentDerbyTable {
	
	@Id
	private int id;
	private String name;
	private String dob;
	private float cgpa;
	private String placed;
	@ManyToOne
	private Company company;
	
}
