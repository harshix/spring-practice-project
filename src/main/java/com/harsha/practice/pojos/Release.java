package com.harsha.practice.pojos;

import java.util.List;

public class Release {
	
	private String date;
	private List<String> languages;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public List<String> getLanguages() {
		return languages;
	}
	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}
	
	
}
