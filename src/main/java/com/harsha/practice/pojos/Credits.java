package com.harsha.practice.pojos;

import java.util.List;

public class Credits {

	private String director;
	private List<String> production;
	private String music;
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public List<String> getProduction() {
		return production;
	}
	public void setProduction(List<String> production) {
		this.production = production;
	}
	public String getMusic() {
		return music;
	}
	public void setMusic(String music) {
		this.music = music;
	}
	
	
	
	
}
