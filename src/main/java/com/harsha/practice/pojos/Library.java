package com.harsha.practice.pojos;

import java.util.List;

public class Library {

	private List<Language> languages;

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}
	
	
}
