package com.harsha.practice.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "movie")
public class MovieMySQLTable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String name;
	private String director;
	@Column(name = "release_date")
	private String releaseDate;
	@Column(name = "music_director")
	private String musicDirector;
	private int runtime;
	
	public MovieMySQLTable(int int1, String string, String string2, String string3, String string4, int int2) {
		this.id = int1;
		this.name = string;
		this.director = string2;
		this.releaseDate = string3;
		this.musicDirector = string4;
		this.runtime = int2;
	}
	
}
