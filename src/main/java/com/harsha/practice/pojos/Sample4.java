package com.harsha.practice.pojos;


import java.util.List;

import lombok.Data;
import lombok.Getter;

@Data
public class Sample4 {

	@Getter
	private String page;
	private String per_page;
	private String total;
	private String total_pages;
	@Getter
	private List<Sample4List> data;
	
}
