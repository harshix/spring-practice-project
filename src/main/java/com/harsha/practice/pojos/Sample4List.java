package com.harsha.practice.pojos;

import lombok.Data;

@Data
public class Sample4List {

	private String id;
	private String name;
	private String year;
	private String color;
	private String pantone_value;
	private String email;
	private String first_name;
	private String last_name;
	private String avatar;
}
