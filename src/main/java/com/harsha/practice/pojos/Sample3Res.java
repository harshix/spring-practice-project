package com.harsha.practice.pojos;

import lombok.Data;
import lombok.Getter;

@Data
public class Sample3Res {

	private String id;
	@Getter
	private String token;
}
