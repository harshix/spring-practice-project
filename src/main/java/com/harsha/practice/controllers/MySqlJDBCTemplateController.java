package com.harsha.practice.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.harsha.practice.pojos.MovieMySQLTable;
import com.harsha.practice.repositories.MovieMySQLwithJDBCTemplateRepository;

@RestController
public class MySqlJDBCTemplateController {

	@Autowired
	MovieMySQLwithJDBCTemplateRepository movieService;
	
	@RequestMapping("mySql/jdbc/getAll")
	public List<MovieMySQLTable> getAll() {
		return movieService.findAllMovies();
	}
	
	@RequestMapping("mySql/jdbc/getMovieById/{id}")
	public MovieMySQLTable getMovieById(@PathVariable int id) {
		return movieService.findMovieById(id);
	}
	
	@PostMapping("mySql/jdbc/addMovie")
	@ResponseStatus(HttpStatus.CREATED)
	public String addMovieBy(@RequestBody MovieMySQLTable m) {
		return "No. of rows inserted : " +movieService.insertMovie(m);
	}
	
}
