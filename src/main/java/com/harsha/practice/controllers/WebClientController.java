package com.harsha.practice.controllers;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.harsha.practice.pojos.Sample3Req;
import com.harsha.practice.pojos.Sample3Res;
import com.harsha.practice.pojos.Sample4;
import com.harsha.practice.pojos.Sample4List;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class WebClientController {

	//??? diferrence between http:// and https:// w.r.t. calling application 
	
	@RequestMapping(method=RequestMethod.POST, value="/rest/mono")
	public Mono<String> getHttpMonoResponse(@RequestBody Sample3Req request) {
		WebClient webClient = WebClient.create("https://reqres.in");
		return webClient.post()
			.uri("/api/register")
			.bodyValue(request)
			.retrieve()
			.bodyToMono(Sample3Res.class)
			.flatMap(f-> {
				return Mono.just(f.getToken());
				});
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/rest/flux1")
	public Flux<Sample3Res> getHttpFlux1Response(@RequestBody Sample3Req request) {
		WebClient webClient = WebClient.create("https://reqres.in");
		return webClient.post()
			.uri("/api/register")
			.bodyValue(request)
			.retrieve()
			.bodyToFlux(Sample3Res.class)
			.flatMap(f-> { 
				return Flux.just(f,f); 
				});
	}
	
	// ??? how to avoid hardcode of indexes below
	
	@RequestMapping(method=RequestMethod.GET, value="/rest/flux2")
	public Flux<Sample4List> getHttpFlux2Response() {
		WebClient webClient = WebClient.create("https://reqres.in");
		return webClient.get()
			.uri("/api/unknown")
			.retrieve()
			.bodyToFlux(Sample4.class)
			.flatMap(f-> { 
				return Flux.just(f.getData().get(0),f.getData().get(1)); 
				});
	}
}
