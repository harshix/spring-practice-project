package com.harsha.practice.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.harsha.practice.pojos.Sample4;

@RestController
public class ExecutorOperationController {

	@RequestMapping("/executor/method1")
	public HashMap<String,Date> getExecutorResponse() {
		
		ExecutorService executor = Executors.newFixedThreadPool(10);
		HashMap<String,Date> resp = new HashMap<String,Date>();
		
		Future<HashMap<String,Date>> resp1 = executor.submit(task1);
		Future<HashMap<String,Date>> resp2 = executor.submit(task2);
		
		try {
			resp.putAll(resp1.get());
			resp.putAll(resp2.get());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return resp;
	}
	
	Callable<HashMap<String,Date>> task1 = () -> {
		RestTemplate rest = new RestTemplate();
		System.out.println("Task1 execution start");
		Sample4 response = rest.getForObject("https://reqres.in/api/users?delay=3", Sample4.class);
		System.out.println("Task1 execution end");
		Date now = new Date();
		HashMap<String,Date> responseMap = new HashMap<String,Date>();
		responseMap.put("Task2", now);
		return responseMap;
	};
	
	Callable<HashMap<String,Date>> task2 = () -> {
		RestTemplate rest = new RestTemplate();
		System.out.println("Task2 execution start");
		Sample4 response = rest.getForObject("https://reqres.in/api/users?delay=2", Sample4.class);
		System.out.println("Task2 execution end");
		Date now = new Date();
		HashMap<String,Date> responseMap = new HashMap<String,Date>();
		responseMap.put("Task1", now);
		return responseMap;
	};
	
}
