package com.harsha.practice.controllers;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.harsha.practice.pojos.Credits;
import com.harsha.practice.pojos.Release;
import com.harsha.practice.services.PropertyService;

@RestController
public class PropertyController {
	
	/* ??? why is autowired needed, shouldn't Value kick-in without autowire?*/
	/* ??? if autowired, what constructors kick in?*/
	/* ??? how to read the custom.properties file? */
	
	@Autowired 
	public PropertyService propertyObj;
	
	@RequestMapping("/movie/cast")
	public HashMap<String, String> getMovieCast(String movie) {
		return propertyObj.getCast();
	}
	
	@RequestMapping("/movie/credits")
	public Credits getMovieCredits() {
		return propertyObj.getCredits();
	}
	
	@RequestMapping("/movie/release")
	public Release getMovieRelease() {
		return propertyObj.getRelease();
	}
	
	@RequestMapping("/movie/actor/{actor}")
	public String getMovieCharacter(@PathVariable String actor) {
		return propertyObj.getCharacter(actor);
	}
	
	@RequestMapping("/movie/getName")
	public String getMovieName() {
		return propertyObj.getMovie();
	}
	
	@RequestMapping("/movie/getLanguage")
	public String getMovieLanguage() {
		return propertyObj.getLanguage();
	}
	
}
