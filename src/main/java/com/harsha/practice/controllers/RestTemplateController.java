package com.harsha.practice.controllers;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.harsha.practice.pojos.PostRequest;
import com.harsha.practice.pojos.Sample1;
import com.harsha.practice.pojos.Sample2;

@RestController
public class RestTemplateController {

	@RequestMapping(method=RequestMethod.GET, value="/rest/get/{id}")
	public ResponseEntity<Sample1> getHttpGetResponse(@PathVariable String id) {
		RestTemplate rest = new RestTemplate();
		Sample1 response = rest.getForObject("https://jsonplaceholder.typicode.com/todos/"+Integer.parseInt(id), Sample1.class);
		ResponseEntity<Sample1> resp = new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		return resp;
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/rest/post")
	public ResponseEntity<Sample2> getHttpPostResponse(@RequestBody PostRequest postReq, @RequestHeader MultiValueMap<String, String> headers) {
		RestTemplate rest = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.addAll(headers);
		HttpEntity<PostRequest> httpEntity = new HttpEntity<>(postReq, httpHeaders);
		ResponseEntity<Sample2> response = rest.postForEntity("https://reqres.in/api/users", httpEntity, Sample2.class);
		return response;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/rest/map/{id}")
	public Sample1 getHttpMapResponse(@PathVariable String id) {
		RestTemplate rest = new RestTemplate();
		String response = rest.getForObject("https://jsonplaceholder.typicode.com/todos/"+Integer.parseInt(id), String.class);
		ObjectMapper mapper = new ObjectMapper();
		Sample1 resp = new Sample1();
		try {
			resp = mapper.readValue(response, Sample1.class);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}
	
	
}
