package com.harsha.practice.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.harsha.practice.pojos.MovieMySQLTable;
import com.harsha.practice.services.MovieOnMySQLwithJPAService;

@RestController
public class MySQLJPAController {

	@Autowired
	MovieOnMySQLwithJPAService movieService;
	
	@RequestMapping("/mySQL/getAll")
	public List<MovieMySQLTable> getAllMovies() {
		return movieService.getAll();
	}
	
	@RequestMapping("/mySQL/getMovieById/{id}")
	public MovieMySQLTable getMovieById(@PathVariable int id) {
		return movieService.getMovie(id);
	}
	
	@PostMapping("/mySQL/addMovie")
	@ResponseStatus(HttpStatus.CREATED)
	public void addMovie(@RequestBody MovieMySQLTable m) {
		movieService.addMovie(m);
	}
	
	@PostMapping("/mySQL/updateMovie")
	@ResponseStatus(HttpStatus.CREATED)
	public void updateMovie(@RequestBody MovieMySQLTable m) {
		movieService.updateMovie(m);
	}
	
	@DeleteMapping("/mySQL/deleteMovieById/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void deleteMovie(@PathVariable int id) {
		movieService.deleteMovie(id);
	}
}
