package com.harsha.practice.controllers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.harsha.practice.pojos.Student;
import com.harsha.practice.services.StudentService;

@RestController
public class StreamOperationController {
	
	@Autowired
	StudentService studentObj;
	
	@RequestMapping("/student/all")
	public List<Student> getAllStudents() {
		return studentObj.getStudents();
	}
	
	@RequestMapping("/student/id/{id}")
	public Student getStudentById(@PathVariable String id) {
		return studentObj.getStudent(Integer.parseInt(id));
	}
	
	@RequestMapping("/student/name/{name}")
	public Student getStudentByName(@PathVariable String name) {
		return studentObj.getStudents().stream().filter(s->s.getName().equalsIgnoreCase(name)).findFirst().get();
	}
	
	@RequestMapping("/student/id/{id}/name")
	public String getStudentNameById(@PathVariable String id) {
		return studentObj.getStudents().stream().filter(s->s.getId()==Integer.parseInt(id)).findFirst().map(s->s.getName()).get();
	}
	
	@RequestMapping("/student/all/ranking")
	public List<Student> getStudentsRank() {
		return studentObj.getStudents().stream().sorted(Comparator.comparing(Student::getRank)).collect(Collectors.toList());
		//return studentObj.getStudents().stream().sorted((s1,s2)->Integer.compare(s1.getRank(), s2.getRank())).collect(Collectors.toList());
	}
	
	@RequestMapping("/student/all/order")
	public List<Student> getStudentsName() {
		return studentObj.getStudents().stream().sorted((s1,s2)->s1.getName().compareToIgnoreCase(s2.getName())).collect(Collectors.toList());
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/student/add")
	@ResponseStatus(HttpStatus.CREATED)
	public void addStudent(@RequestBody Student student) {
		studentObj.getStudents().add(student);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/student/update")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void updateStudent(@RequestBody Student student) {
		int i = studentObj.getStudents().stream().map(s->s.getId()).collect(Collectors.toList()).indexOf(student.getId());
		studentObj.getStudents().set(i, student);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/student/delete/id/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void deleteStudent(@PathVariable String id) {
		studentObj.getStudents().removeIf(s->s.getId()==Integer.parseInt(id));
	}
	
}
