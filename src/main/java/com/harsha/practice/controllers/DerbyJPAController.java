package com.harsha.practice.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.harsha.practice.pojos.Company;
import com.harsha.practice.pojos.StudentDerbyTable;
import com.harsha.practice.repositories.CompanyDerbyRepository;
import com.harsha.practice.services.StudentDerbyService;

@RestController
public class DerbyJPAController {

	@Autowired
	StudentDerbyService studentDerbyService;
	
	@Autowired
	CompanyDerbyRepository companyInterface;
	
	@RequestMapping("/derby/getAll")
	public List<StudentDerbyTable> getAllStrdents() {
		return studentDerbyService.getAllStudents();
	}
	
	@RequestMapping("/derby/getId/{id}")
	public StudentDerbyTable getStudentById(@PathVariable int id) {
		return studentDerbyService.getStudent(id);
	}
	
	@PostMapping("/derby/addStudent")
	@ResponseStatus(HttpStatus.CREATED)
	public void addStudent(@RequestBody StudentDerbyTable s) {
		Company c = new Company();
		c.setName(s.getPlaced());
		if (companyInterface.existsById(s.getPlaced()))
			c.setIndustry(companyInterface.findById(s.getPlaced()).get().getIndustry());
		else 
			c.setIndustry("NULL");
			companyInterface.save(c);
		s.setCompany(c);
		studentDerbyService.addStudent(s);
	}
	
	@PostMapping("/derby/updateStudent")
	@ResponseStatus(HttpStatus.CREATED)
	public void updateStudent(@RequestBody StudentDerbyTable s) {
		studentDerbyService.addStudent(s);
	}
	
	@DeleteMapping("/derby/getId/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void deleteStudentById(@PathVariable int id) {
		studentDerbyService.deleteStudent(id);
	}
	
	@GetMapping("/derby/getByName/{name}")
	public List<StudentDerbyTable> getStudentByName(@PathVariable String name) {
		return studentDerbyService.getStudentByName(name);
	}
	
	@PostMapping("/derby/addCompany")
	@ResponseStatus(HttpStatus.CREATED)
	public void addCompany(@RequestBody Company c) {
		companyInterface.save(c);
	}
	
	@GetMapping("/derby/getByCompany/{name}")
	public List<StudentDerbyTable> getStudentsByCompany(@PathVariable String name) {
		return studentDerbyService.getStudentsByCompanyName(name);
	}
	
	@GetMapping("/derby/getCompany/{id}")
	public Company getCompanyByStudentId(@PathVariable int id) {
		return companyInterface.findById(studentDerbyService.getStudent(id).getPlaced()).get();
	}
	
}
