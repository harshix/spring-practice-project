package com.harsha.practice.repositories;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.harsha.practice.pojos.MovieMySQLTable;

@Repository
public class MovieMySQLwithJDBCTemplateRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public List<MovieMySQLTable> findAllMovies() {
		List<MovieMySQLTable> movieList = new ArrayList<>();
		movieList =	jdbcTemplate.query("select * from movie;", (rs, rowNum) -> {
					return new MovieMySQLTable(rs.getInt("id"),rs.getString("name"),rs.getString("director"),rs.getString("release_date"),rs.getString("music_director"),rs.getInt("runtime"));
					});
		return movieList;
	}
	
	public MovieMySQLTable findMovieById(int id) {
		return jdbcTemplate.queryForObject("select * from movie where id="+id+";", (rs, rowNum) -> {
					return new MovieMySQLTable(rs.getInt("id"),rs.getString("name"),rs.getString("director"),rs.getString("release_date"),rs.getString("music_director"),rs.getInt("runtime"));
					});
	}
	
	public int insertMovie (MovieMySQLTable m) {
		
		return jdbcTemplate.update("insert ignore into movie (id, name, director, release_date, music_director, runtime) values (?,?,?,?,?,?)", m.getId(), m.getName(), m.getDirector(), m.getReleaseDate(),m.getMusicDirector(),m.getRuntime());
	}
	
}
