package com.harsha.practice.repositories;

import org.springframework.data.repository.CrudRepository;

import com.harsha.practice.pojos.Company;

public interface CompanyDerbyRepository extends CrudRepository<Company, String> {

}
