package com.harsha.practice.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.harsha.practice.pojos.Company;
import com.harsha.practice.pojos.StudentDerbyTable;

public interface StudentDerbyRepository extends CrudRepository<StudentDerbyTable, Integer> {

	public List<StudentDerbyTable> findByName(String name);
	public List<StudentDerbyTable> findByCompanyName(String name);
}
