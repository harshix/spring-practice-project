package com.harsha.practice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.harsha.practice.pojos.MovieMySQLTable;

public interface MovieMySQLwithJPARepository extends JpaRepository<MovieMySQLTable, Integer>{

}
