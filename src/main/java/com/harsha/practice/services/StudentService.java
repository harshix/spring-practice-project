package com.harsha.practice.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.harsha.practice.pojos.Course;
import com.harsha.practice.pojos.Student;
import com.harsha.practice.pojos.Subject;

@Service
public class StudentService {

	private List<Subject> subjects = Arrays.asList(new Subject("Maths","Shankar"), new Subject("Physics","Vikram"), new Subject("Chemistry","Chandra"));
	private Course course = new Course("MPC",this.subjects);
	private List<Student> students = new ArrayList<>(Arrays.asList(	 new Student (1, "Harsha", this.course, 606)
										   			,new Student (2, "Goutham", this.course, 745)
									   				,new Student (3, "Aditya", this.course, 501)
									   				,new Student (4, "Anvesh", this.course, 4003)
									   				,new Student (5, "Anudeep", this.course, 25)
									   				,new Student (6, "Ravinder", this.course, 4)
						   		));
	
	public List<Student> getStudents() {
		return this.students;
	}
	
	public Student getStudent(int id) {
		return this.students.stream().filter(s-> s.getId()==id).findFirst().get();
	}
	
}
