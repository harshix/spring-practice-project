package com.harsha.practice.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.harsha.practice.pojos.MovieMySQLTable;
import com.harsha.practice.repositories.MovieMySQLwithJPARepository;

@Service
public class MovieOnMySQLwithJPAService {

	@Autowired
	MovieMySQLwithJPARepository movieMySQLInterface;
	
	public List<MovieMySQLTable> getAll() {
		List<MovieMySQLTable> movies = new ArrayList<>();
		movieMySQLInterface.findAll().forEach(m->movies.add(m));
		return movies;
	}
	
	public void addMovie(MovieMySQLTable m) {
		movieMySQLInterface.save(m);
	}
	
	public void updateMovie(MovieMySQLTable m) {
		movieMySQLInterface.save(m);
	}
	
	public MovieMySQLTable getMovie(int id) {
		return movieMySQLInterface.findById(id).get();
	}
	
	public void deleteMovie(int id) {
		movieMySQLInterface.deleteById(id);
	}
}
