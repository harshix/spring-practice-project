
package com.harsha.practice.services;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.harsha.practice.pojos.Credits;
import com.harsha.practice.pojos.Release;

@Service
@Configuration
@PropertySource("classpath:custom1.properties")
//VM Argument to be passed -Dspring.config.location=classpath:custom1.properties
@ConfigurationProperties(prefix="library.language.movie")
public class PropertyService {
	
	@Autowired
	private Environment e;
	
	// Set based on @ConfigurationProperties
	private Credits credits;
	
	// Set based on Environment e
	private Release release;
	
	@Value("${library.language}")
	private String language;
	
	@Value("${library.language.movie}")
	private String movie;
	
	@Value("${library.language.movie.actors}")
	private List<String> actors;
	
	@Value("${library.language.movie.characters}")
	private List<String> characters;

	private HashMap<String, String> cast;
	
	public String getLanguage() {  
		return language;                      /* ??? How is this getting value when pointing to yml */
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMovie() {
		return movie;
	}

	public void setMovie(String movie) {
		this.movie = movie;
	}

	public List<String> getActors() {
		return actors;
	}

	public void setActors(List<String> actors) {
		this.actors = actors;
	}

	public List<String> getCharacters() {
		return characters;
	}

	public void setCharacters(List<String> characters) {
		this.characters = characters;
	}

	public HashMap<String, String> getCast() {
		return this.prepareCast();
	}

	public void setCast(HashMap<String, String> cast) {
		this.cast = cast;
	}
	
	private HashMap<String, String> prepareCast() {
		HashMap<String, String> cast = new HashMap<String, String>(); /* ??? why is new object required? can't we use existing cast*/
		for (int i=0;i<this.actors.size();i++) {
			cast.put(this.actors.get(i).trim(), this.characters.get(i).trim());
		}
		return cast;
	}

	public Credits getCredits() {
		return credits;
	}

	public void setCredits(Credits credits) {
		this.credits = credits;
	}

	public Release getRelease() {
		Release release = new Release();
		release.setDate(e.getProperty("release.date"));
		release.setLanguages(Arrays.asList(e.getProperty("release.language").split(",")));
		return release;
	}

	public void setRelease(Release release) {
		this.release = release;
	}

	public String getCharacter(String actor) {
		return this.prepareCast().get(actor);
	}
	
	
	
}
