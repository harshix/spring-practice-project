package com.harsha.practice.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.harsha.practice.pojos.StudentDerbyTable;
import com.harsha.practice.repositories.StudentDerbyRepository;

@Service
public class StudentDerbyService {

	@Autowired
	StudentDerbyRepository studentInterface;
	
	public List<StudentDerbyTable> getAllStudents() {
		List<StudentDerbyTable> students = new ArrayList<>();
		studentInterface.findAll().forEach(f->students.add(f));
		return students;
	}
	
	public void addStudent(StudentDerbyTable s) {
		studentInterface.save(s);
	}
	
	public StudentDerbyTable getStudent(int id) {
		return studentInterface.findById(id).get();
	}
	
	public void deleteStudent(int id) {
		studentInterface.deleteById(id);
	}
	
	public List<StudentDerbyTable> getStudentByName(String name) {
		return studentInterface.findByName(name);
	}
	
	public List<StudentDerbyTable> getStudentsByCompanyName(String name) {
		return studentInterface.findByCompanyName(name);
	}
	
}
